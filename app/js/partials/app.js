/******
 * WOW js init
 *******/

var wow = new WOW({
    boxClass: 'wow',
    offset: 0,
    mobile: true,
    live: true
})


jQuery(document).ready(function($) {

    wow.init();


    /* ticket slider */

    var slider = $('.ticket-slider');

    slider.slick({
        dots: false,
        infinite: false,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [{
            breakpoint: 1300,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
                infinite: true,
                dots: false
            }
        },
            {
                breakpoint: 1299,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true,
                    dots: false
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true,
                    dots: false
                }
            },
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }

        ]
    });


    //open popup
    $('.cd-popup-trigger').on('click', function(event) {
        event.preventDefault();
        $('body').toggleClass('overflow-hidden');
        var target = $(this).data('menu');
        $('#' + target).addClass('is-visible');
    });

    //close popup
    $('.cd-popup').on('click', function(event) {
        if ($(event.target).is('.cd-popup-close') || $(event.target).is('.cd-popup')) {
            event.preventDefault();
            $(this).removeClass('is-visible');
            $('body').removeClass('overflow-hidden');
        }
    });
    //close popup when clicking the esc keyboard button
    $(document).keyup(function(event) {
        if (event.which == '27') {
            $('.cd-popup').removeClass('is-visible');
            $('body').removeClass('overflow-hidden');
        }
    });


    /********
     * Cats Slider
     ********/

    enquire.register("(max-width: 767px)", {
        match: function() {
            $('.cats-slider').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
                dots: false,
                responsive: [{
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: false
                    }
                },
                    {
                        breakpoint: 478,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            infinite: true,
                            dots: false
                        }
                    }
                ]
            });


            var slider = $('.artist-slider');

            slider.each(function () {
                var slidesCount = $(this).attr('data-count');
                console.log(slidesCount);
                if (slidesCount > 2 && $(this).length) {
                    $(this).slick({
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: false,
                        responsive: [{
                            breakpoint: 767,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 1,
                                infinite: true,
                                dots: false
                            }
                        },
                            {
                                breakpoint: 478,
                                settings: {
                                    slidesToShow: 1,
                                    slidesToScroll: 1,
                                    infinite: true,
                                    dots: false
                                }
                            }
                        ]
                    });
                }
            });


            $('button.b').click(function() {
                var heightB = $(this).closest('.body-expand-container').height();
                if (!$(this).closest('.body-expand-container.open').length) {
                    console.log('click');
                    $(this).closest('.body-expand-container')
                        .addClass('open')
                        .css('height', 'auto');
                    $(this).closest('.body-expand-container-hide')
                        .toggleClass('ex-close')
                        .addClass('ex-open');
                } else {
                    $(this).closest('.body-expand-container')
                        .removeClass('open')
                        .css('height', '105px');
                    $(this).closest('.body-expand-container-hide')
                        .removeClass('ex-open')
                        .toggleClass('ex-close');
                }

                if ($(this).closest('.body-expand-container.open').length) {
                    $(this).children('i')
                        .removeClass()
                        .toggleClass('fa fa-angle-up');
                } else {
                    $(this).children('i')
                        .removeClass()
                        .toggleClass('fa fa-angle-down');
                }
            });
        },
        unmatch: function() {
            $('.cats-slider').slick('unslick');
            $('.artist-slider').slick('unslick');
        }
    });

    $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
        disableOn: 700,
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,
        fixedContentPos: false
    });

    $("#sharePopup, .sharePopup").jsSocials({
        shareIn: "popup",
        shares: ["facebook", "twitter", "googleplus"]
    });

    $('.popup-gallery').magnificPopup({
        delegate: 'a',
        type: 'image',
        tLoading: 'Loading image #%curr%...',
        mainClass: 'mfp-img-mobile',
        closeBtnInside:false,
        gallery: {
            enabled: true,
            navigateByImgClick: true,
        }
    });


    /* stars func */

    (function () {
        var targetUl = $('#starsUl li');
        var elVal = $('#rating').html();
        if (!targetUl.hasClass('selected')) {
            for (var i = 0; i < Math.floor(elVal); i++) {
                $(targetUl[i]).addClass('selected');
            }
        }
    }());

    $('#starsUl li').on('mouseover', function () {
        var onStar = parseInt($(this).data('value'));

        $(this).parent().children('li.star').each(function (e) {
            if (e < onStar) {
                $(this).addClass('hover');
            } else {
                $(this).removeClass('hover');
            }
        });

    }).on('mouseout', function () {
        $(this).parent().children('li.star').each(function (e) {
            $(this).removeClass('hover');
        });
    });

    /* init tab to slider */
    (function($) {
        fakewaffle.responsiveTabs(['xs', 'sm']);
    })(jQuery);

    /* back to top btn */

    $(".back-top.btn").click(function(s) {
        s.preventDefault();
        $("body,html").animate({
            scrollTop: 0
        }, 500);
    });

    /* Search */
    function ajaxSerach(el,url){
        $(el).keyup(function() {
            $.trim($(this).val());
            $.ajax({
                url: "search.json",
                dataType: "json",
                success: function(response) {
                    $(".concert-search").fadeIn();
                    $(".concert-search ul li").remove();
                    if (response.success === 1 ){
                        $.each(response.items, function(i, data) {
                            if (data && data !== undefined && data !== null ) {
                                $(".concert-search ul").append('<li><a href="' + data.url + '">' + data.title + "</a></li>");
                            } else{
                                $(".concert-search ul").append("<li>Ssory, nothing found</li>");
                            }
                        });
                    } else {
                        $(".concert-search ul").append("<li>Ssory, nothing found</li>");
                    }
                }
            })
        });

        $(el).blur(function() {
            if($(this).val() === ""){
                $(".concert-search").fadeOut();
                $(".concert-search ul li").remove();
            }
        });
    }


    ajaxSerach("#banner-form-search","search.json");
    ajaxSerach("#search-category","search.json");


    /* ajax cats on main */

    var btnContainer = $(".js-cat-list");
    $(document).on("click", "#see-more-cats", function() {
        var offset = $(this).attr("data-offset");
        $.ajax({
            url: "cats.json?offset=" + offset,
            dataType: "json",
            method: "GET",
            beforeSend: function() {},
            success: function(response) {
                $("html, body").animate({
                    scrollTop: $(".js-cat-list").offset().top + $(".js-cat-list").height()
                }, 1200);
                buidCats(response.items, btnContainer);
                $("#see-more-cats").attr("data-offset", parseInt(offset) + 4);
            }
        })
    });

    function buidCats(json, el) {
        $("#catsItem").tmpl(json).appendTo(el);
    }

});




